# Windows

> My Windows installation and configuration *screenplay* (not *script*, I'm not there yet and I'm not sure it can be even fully scripted).

## Tips

### Local DNS Rewrite

Use `hosts` file (`C:\Windows\System32\drivers\etc\hosts`) to set up a host name with alternative resolution:

```
127.0.0.1 test.example.com
```

`test.example.com` will now resolve to `localhost` (all port numbers).

If you still want to preserve easy access to the original host name, map it to its IP address like this:

```
127.0.0.1 test.example.com
1.2.3.4 test.realexample.com
```

Where `1.2.3.4` is the global DNS response for the `test.realexample.com` host name.
HTTPS certificates will continue working without a conflict.

In case you wantt to limit this to a certain port number, use Fiddler instead of the `hosts` file:

https://stackoverflow.com/a/21586034/2715716

## Installing Windows

- Download and run the [Windows 10 Media Creation tool](https://www.microsoft.com/en-us/software-download/windows10)
- Make an ISO with English (US) Windows 10 x64 image and burn it to a flash drive
- [ ] Figure out how to patch this to be able to run the installation unattended and run post-install PowerShell scripts
- Boot to the flash drive and proceed with the installation disabling all telemetry and configuring only US Querty keyboard keyboard
- Connect to the wi-fi to install pending updates and let Windows 10 download hardware drivers

## Decluttering Windows

- [ ] Script this based on the PowerShell scripts in the To Do section

## Configuring Windows

### Desktop > *Display settings*

- Right-click on the desktop and select *Display settings*, change *Change the size of text, apps, and other items* to *100 %* and restart
- Set up relevant world clocks
- Go to the *Recycle Bin*, *Manage*, *Recycle Bin Properties*, *Don't move files*, *Apply*
- Remove all desktop icons and disable showing desktop icons by right-clicking the desktop and selecting *View*, unchecking *Show desktop icons*

### Taskbar

- Hide Cortana: right-click the taskbar, expand *Cortana*, select *Hidden*
- Hide task view button: right-click the taskbar, uncheck *Show Task View button*
- Hide people button: right-click on the taskbar, uncheck *Show People on the taskbar*
- Unpin Mail, Sotre & Edge from taskbar
- Pin PowerShell
- Pin Explorer
- Pin Control Panel
- Pin Firefox
- Pin Chrome
- Pin Edge
- Pin Spotify
- Pin Visual Studio Code
- Pin Visual Studio
- Pin Notepad
- Pin Ubuntu
- Pin Paint .NET
- Pin Hyper-V

### Notification Center

- Dismiss the Cortana notification in the notification center using the geat by clicking *Turn off notifications for Cortana*
- Expand the control bar in the notification center using the *Expand* button

### Notification area

- Find the Bluetooth icon, right-click and select *Remove icon* (can always search for it)
- Find the *Actions recommended* Defender notification icon, right-click it and select *View notification options*, uncheck *Get informational notifications*

### Task Manager

- Click on *More details* to default to the full view
- Disable *Windows Defender notification* in the *Startup* tab
- Disable *Microsoft OneDrive* in the *Startup* tab
- Disable anything else suspicious except *QuickSet* which I think is for Dell special keys (function keys alternative functions)

### Start menu

- Clear by unpinning and collapse all extra start menu columns
- Uninstall *Tips*
- Uninstall *Spotify*  (will install the Desktop app myself)
- Uninstall *Remote Desktop*
- Uninstall *Power Director* (Dell shit, won't be there with OEM installation from the Media Creation tool)
- Uninstall *Power2Go* (Dell shit, won't be there with OEM installation from the Media Creation tool)
- Uninstall *Power Media Player* (Dell shit, won't be there with OEM installation from the Media Creation tool)
- Uninstall *Paid Wi-Fi & Cellular*
- Uninstall *My Office*
- Uninstall *Media Suite*
- Disable *Occasionally show suggestions*
- Remove pre-installed apps:

```powershell
# Alarms
Get-AppxPackage Microsoft.WindowsAlarms | Remove-AppxPackage
# Bubble Witch
Get-AppxPackage *BubbleWitch* | Remove-AppxPackage
# Calculator
Get-AppxPackage Microsoft.WindowsCalculator | Remove-AppxPackage
# Candy Crush
Get-AppxPackage king.com.CandyCrush* | Remove-AppxPackage
# Dell Product Registration
Get-AppxPackage DellInc.DellProductRegistration | Remove-AppxPackage
# Feedback Hub
Get-AppxPackage Microsoft.WindowsFeedbackHub | Remove-AppxPackage
# Finance
Get-AppxPackage Microsoft.BingFinance | Remove-AppxPackage
# Get help
Get-AppxPackage Microsoft.Gethelp | Remove-AppxPackage
# Groove Music
Get-AppxPackage Microsoft.ZuneMusic | Remove-AppxPackage
# Mail & Calendar
Get-AppxPackage microsoft.windowscommunicationsapps | Remove-AppxPackage
# March of Empires
Get-AppxPackage *MarchofEmpires* | Remove-AppxPackage
# Messaging
Get-AppxPackage Microsoft.Messaging | Remove-AppxPackage
# Minecraft
Get-AppxPackage *Minecraft* | Remove-AppxPackage
# Mixed Reality Portal
# https://www.askvg.com/tip-how-to-uninstall-mixed-reality-portal-in-windows-10/
Get-AppxPackage *HolographicFirstRun* | Remove-AppxPackage
# Mixed Reality Viewer
Get-AppxPackage Microsoft.Microsoft3DViewer | Remove-AppxPackage
# Movies & TV
Get-AppxPackage Microsoft.ZuneVideo | Remove-AppxPackage
# News
Get-AppxPackage Microsoft.BingNews | Remove-AppxPackage
# OneNote
Get-AppxPackage Microsoft.Office.OneNote | Remove-AppxPackage
# Paint 3D
Get-AppxPackage *Microsoft.MSPaint* | Remove-AppxPackage
# People
Get-AppxPackage Microsoft.People | Remove-AppxPackage
# Phone Companion
Get-AppxPackage Microsoft.WindowsPhone | Remove-AppxPackage
# Photos
Get-AppxPackage Microsoft.Windows.Photos | Remove-AppxPackage
# Sketchbook
Get-AppxPackage *AutodeskSketchBook | Remove-AppxPackage
# Skype
Get-AppxPackage Microsoft.SkypeApp | Remove-AppxPackage
# Solitaire
Get-AppxPackage Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage
# Sports
Get-AppxPackage Microsoft.BingSports | Remove-AppxPackage
# Sticky Notes
Get-AppxPackage Microsoft.MicrosoftStickyNotes | Remove-AppxPackage
# Voice Recorder
Get-AppxPackage Microsoft.WindowsSoundRecorder | Remove-AppxPackage
# Weather
Get-AppxPackage Microsoft.BingWeather | Remove-AppxPackage
# XBox
Get-AppxPackage Microsoft.XboxApp | Remove-AppxPackage
Get-AppxPackage Microsoft.XboxIdentityProvider | Remove-AppxPackage
```

### Settings

- Make Mozilla Firefox the default browser
- Enable Developer Mode

### Control Panel

- Go to Programs and Features and uninstall OneDrive
- Enable Hyper-V in *Turn Windows features on and off* and restart
- Enable Windows Subsystem for Linux in *Turn Windows features on and off* and restart (`Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux`)
- Go to Windows Store and install Ubuntu, ensure `ubuntu` and `wsl` works
- Refer to the Secret Management post in order to understand keeping secrets
  - [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-secret-management)
  - [Bloggo post](http://hubelbauer.net/post/secret-management)
  - `gpg -c credentials.md > credentials.md.pgp` to encrypt a file
  - `gpg -d credentials.md.gpg > credentials.md` to decrypt a file
  - Do not forget to remove the cleartext file after each change :-)
- Disable going to sleep at lid close by setting everything to *Do nothing* in *System & Security* > *Power Options* > *Choose what closing the lid does*
- Disable Remote Assistence in *System & Security* > *Allow remote access to this computer*

### Explorer

- Press Ctrl + F1 in Explorer to permanently expand the ribbon menu
- Close the OneDrive advertisement
- *File* > *Change folder and search options*
    - Set the default explorer view by setting *Open file explorer to* to *This PC* on the *General* tab
    - Configure the *View* tab
        - Uncheck not showing hidden folders
        - Uncheck hiding empty drives
        - Uncheck *Hide extensions for known file types*
        - Uncheck *Hide protected operating system files (Recommended)*
        - Uncheck *Show sync provider notifications*
        - Check *Launch folder windows in a separate process*

## Installing software

- [ ] Use Chocolatey to script installation and configuration of these

### Mozilla Firefox

- Download and install Firefox without making it the default browser
  - Go to `about:preferences#privacy` and disallow anti-privacy bullshit
  - Skip this step when asked about Firefox Sync
  - Reject importing stuff with *No thanks*

### Google Chrome

- Go to `https://www.google.com/chrome/` in Edge and download and install Chrome rejecting the Make spying better shit
- Go to `https://google.com/ncr` in Chrome enough times for Chrome to suggest it when typing `g`
- Reject the Chrome sign in with *No thanks*
- Install [uBlock Origin](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) by Raymond Hill
- Install [HN Enhancement Suite](https://chrome.google.com/webstore/detail/hacker-news-enhancement-s/bappiabcodbpphnojdiaddhnilfnjmpm) by etcet.net
- Prevent Chrome from translating things: `chrome://settings/languages`
- Sign into:
    - Fastmail
    - WhatsApp Web
    - YouTube
    - Hacker News
    - Last.fm 
- Sign in to services for work:
    - [Outlook](https://login.microsoftonline.com)
    - Jira
    - Stash
    - Slack
    - Timetastic
- Pin tabs:
    - Fastmail
    - WhatsApp Web
    - To do do

### Git

- Go to `https://git-scm.com/download/win` and download and install Git
  - No desktop icon
  - No "Git shit here" in context menus
  - No "associate shell files with Bash"
  - Yes to "Check daily for Git updates"
  - Use VS Code as Git editor
  - Use Git from Windows command prompt
  - Use OpenSSL
  - Checkout as-is, commit as-is
  - Use Windows' default console window
  - Leave the Enable page as-is
- Open Git Bash and run `ssh-keygen` with no passphrase (do the same for WSL if expected to use WSL to commit)
- Set Git username and email

### Microsoft Visual Studio

- Make sure Git is installed

### Microsoft Visual Studio Code

- Download and install Fira Code
- Make sure Git is installed
- Go directly to `https://code.visualstudio.com` to download and install VS Code
  - Create Desktop icon
  - Add both "Open with Code"
  - "Register Code"
  - Hide Open Editors thingy
- Install extensions:
    - `PeterJausovec.vscode-docker`
    - `rust-lang.rust`
    - `reznikdi.windbg-debug`
    - `eamodio.gitlens`
    - `DavidAnson.vscode-markdownlint`
    - [My extensions](https://gitlab.com/TomasHubelbauer/bloggo-vscode#my-extensions)
- Restore settings:
    - `"window.restoreWindows": "one"` to prevent VS Code from opening the last folder
    - [ ] Pull out the rest below like this and comment on it, discourage copy-paste from the code block to force maintenance as opposed to mindless replacement

```json
{
    "explorer.confirmDelete": false,
    "terminal.integrated.shell.windows": "C:\\WINDOWS\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
    "git.confirmSync": false,
    "git.autofetch": true,
    "files.autoSave": "onFocusChange",
    "editor.fontFamily": "Fira Code",
    "editor.fontLigatures": true,
    "editor.renderWhitespace": "boundary",
    "editor.formatOnSave": true,
    "editor.formatOnType": true,
    "editor.minimap.showSlider": "always",
    "editor.mouseWheelZoom": true,
    "editor.showFoldingControls": "always",
    "editor.stablePeek": true,
    "window.newWindowDimensions": "maximized",
    "window.restoreFullscreen": true,
    "window.restoreWindows": "none",
    "window.title": "${dirty}${activeEditorShort}${separator}${rootName}",
    "files.eol": "\n",
    "files.insertFinalNewline": true,
    "explorer.autoReveal": false,
    "explorer.confirmDragAndDrop": false,
    "explorer.openEditors.visible": 0,
    "html.experimental.syntaxFolding": true,
    "html.format.extraLiners": "",
    "html.format.indentInnerHtml": true,
    "html.format.wrapLineLength": 0,
    "html.suggest.angular1": false,
    "html.suggest.ionic": false,
    "json.experimental.syntaxFolding": true,
    "extensions.autoUpdate": false,
    "terminal.integrated.cursorBlinking": true,
    "terminal.integrated.cursorStyle": "line",
    "rust-client.updateOnStartup": true,
    "emmet.excludeLanguages": [
        "markdown",
        "html"
    ],
    "markdownlint.config": {
        "MD007": false,
        "MD034": false
    }
}

```

### Docker

- Install Docker

### Paint .NET

- Install Paint .NET

### Spotify

- Ensure the pre-installed app was uninstalled
- Install and sign into Last.fm

### Slack

The web version of Slack does not support screen sharing.

### Fiddler

### Postman

Chrome Apps are no longer a thing so need to get the desktop version.

## To Do

- [ ] Refer to [Jess Frazelle](https://gist.github.com/jessfraz/7c319b046daa101a4aaef937a20ff41f) for inspiration
- [ ] Refer to [Nick Craver](https://gist.github.com/NickCraver/7ebf9efbfd0c3eab72e9) for inspiration
- [ ] Refer to [Debloat-Windows-10](https://github.com/W4RH4WK/Debloat-Windows-10) for inspiration
- [ ] [How to set up a mirrored volume for file redundancy on Windows 10](https://www.windowscentral.com/how-set-mirrored-volume-file-redundancy-windows-10)
- [ ] Attach my VS Code config
- [ ] See if Chrome profiles+settings are exportable and importable (including extensions?)
- [ ] Document VM setup and explore [Windows 10 Enterprise dev VMs](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines)
